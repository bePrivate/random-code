#include <iostream>

using namespace std;

class ListNode {
  public:
    int val;
    ListNode* p;
    
    ListNode(int passed_val) {
      val = passed_val;
      p = nullptr;
    }
};

class LinkedList {
  public:
    ListNode* head;
    ListNode* tail;
    int len;
    
    void insert_head(int x) {
      ListNode* new_head = new ListNode(x);
      new_head->p = head;
      head = new_head;
      len += 1;
      if (tail == nullptr) {
        tail = head;
      }
    }
    void insert_tail(int x) {
      ListNode* new_tail = new ListNode(x);
      if (tail != nullptr) {
        tail->p = new_tail;
      } 
      tail = new_tail;
      if (head == nullptr) {
        head = tail;
      }
      len += 1;
    }
    void delete_head() {
      if (head != nullptr) {
        ListNode* old_head = head;
        head = head->p;
        delete old_head;
        if (head == nullptr) {
          tail = nullptr;
        }
        len -= 1;
      }
    }
    void delete_tail() {
      if (tail != nullptr) {
        if (head == tail) {
          delete head;
          head = nullptr;
          tail = nullptr;
          len = 0;
        } else {
          ListNode* current_node = head;
          ListNode* prev_node = nullptr;
          while (current_node->p != nullptr) {
            prev_node = current_node;
            current_node = current_node->p;
          }
          delete tail;
          tail = prev_node;
          if (tail != nullptr) {
            tail->p = nullptr;
          }
          len -= 1;
        }
      }
    }
    void insert_at(int n, int x) {
      ListNode* new_node = new ListNode(x);
      ListNode* prev_node = nullptr;
      ListNode* current_node = head;
      for (int i = 0; i < n && current_node != nullptr; i++) {
        prev_node = current_node;
        current_node = current_node->p;
      }
      if (prev_node != nullptr || current_node != nullptr) {
        prev_node->p = new_node;
        new_node->p = current_node;
      }
      len += 1;
    }
    void delete_at(int n) {
      ListNode* prev_node = nullptr;
      ListNode* current_node = head;
      for (int i = 0; i < n; i++) {
        prev_node = current_node;
        current_node = current_node->p;
      }
      if (current_node != nullptr) {
        ListNode* next_node = current_node->p;
        delete current_node;
        prev_node->p = next_node;
      }
      len -= 1;
    }
    int get_at(int n) {
      ListNode* current_node = head;
      
      for (int i = 0; i < n && current_node != nullptr; i++) {
        current_node = current_node->p;
      }

      if (current_node != nullptr) {
        return current_node->val;
      } else {
        return -1;
      }
    }
    void set_at(int n, int x) {
      ListNode* current_node = head;
      
      for (int i = 0; i < n && current_node != nullptr; i++) {
        current_node = current_node->p;
      }

      if (current_node != nullptr) {
        current_node->val = x;
      }
    }
    void show() {
      ListNode* current_node = head;
      
      cout << "[";
      if (head == nullptr) {
        cout << "]";
      } else {
        for (int i = 0; i < len && current_node != nullptr; i++) {
          if (i < len-1) {
            cout << current_node->val << ", ";
            current_node = current_node->p;
          } else {
            cout << current_node->val << "]";
          }
        }
      }
      cout << endl;
    }

    LinkedList() {
      head = nullptr;
      tail = head;
      len = 0;
    }
};

int main() {
  LinkedList ll;

  cout << "initial state of the list" << endl;
  ll.show();
  
  cout << "creating the first version of the list" << endl;
  ll.insert_tail(12);
  ll.insert_tail(11);
  ll.insert_tail(6);
  ll.insert_tail(27);
  ll.insert_tail(2);
  ll.show();

  cout << "deleting the head" << endl;
  ll.delete_head();
  ll.show();

  cout << "deleting the tail" << endl;
  ll.delete_tail();
  ll.show();

  cout << "inserting at the second position" << endl;
  ll.insert_at(1, 5);
  ll.show();

  cout << "the third value" << endl;
  cout << ll.get_at(2) << endl;

  cout << "setting the third value to 33" << endl;
  ll.set_at(2, 33);
  ll.show();

  return 0;
}
